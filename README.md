# rtabmap-orbslam2

Using ORB SLAM2 as an odom source for RTABMap for Mapping, and Navigation.


## Build docker image
1. Clone this repository, and `cd` into it. 
2. Run `docker build -t wp2_rtabmaporb` . 

OR 
1. `docker pull registry.gitlab.inria.fr/spring/dockers/wp2_rtabmap:orb_odom`

## Running it in ARI
1. Follow similar instructions as in [here](https://gitlab.inria.fr/spring/wp6_robot_behavior/robot_behavior), i.e.

* Save image to zip file
* `Scp` zip file --> ARI
* Load image in ARI 

2. Replace the `/home/pal/tweaked/run_rtabmap_docker.sh` with the one in this repo. 
In ARI:
* ssh pal@ari-Xc
* rm /home/pal/tweaked/run_rtabmap_docker.sh
* exit
In your system (repo location):
* `scp run_rtabmap_docker.sh pal@ari-Xc:~/tweaked/`

3. Either start from web commander, or restart ARI to start automatically. 

## Notes:
* Usage is almost exactly as in [here](https://gitlab.inria.fr/spring/wp6_robot_behavior/robot_behavior). The mapping can be visualized in `rviz` by adding the `/slam/grid_map` and the `/robot_pose`, and for the localization, with the `/slam/localization_pose` topics.

* ORB SLAM requires some sort of initialization, so once the docker is running and `/slam/odom` is publishing:
    * The auto exposure is now turned OFF and ON when the docker is run, and this should provide the required initialization. If it's not enough, see below:
        * run `rostopic echo /slam/odom` and see if the values are constant `0` for all parameters or changing. 
        * If it's constant, wave your hand in front of the camera, or turn the lights off and on. Usually, slow motion of the robot is not sufficient for the initialization.

* As a check to see if things are running as they should, run `rosrun rqt_tf_tree rqt_tf_tree` from a base station (with GUI). The tf tree should look like: 
`map --> odom_ORB --> odom --> base_footprint --> base_link -->....`


## Relevant content:
* https://github.com/introlab/rtabmap_ros/issues/350#issuecomment-1259659987
* https://github.com/introlab/rtabmap/issues/909
* https://github.com/introlab/rtabmap/issues/910#issuecomment-1264079058