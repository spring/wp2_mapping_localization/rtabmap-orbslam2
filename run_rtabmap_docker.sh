#!/bin/bash
for i in "$@"
do
case $i in
     -d=*|--database_name=*)
    DATABASE_NAME="${i#*=}"
    ;;    
    *)
    echo "Argument error"
    exit 3
    ;;
esac
done

if [ -z $DATABASE_NAME ]
then
  DATABASE_NAME=default_map
else
  DATABASE_NAME=$DATABASE_NAME
fi

echo DATABASE_NAME=$DATABASE_NAME 
cp /home/pal/maps/$DATABASE_NAME.db /home/pal/maps/running_map.db
chmod +x /home/pal/maps/running_map.db
docker stop wp2_rtabmaporb
sleep 20
docker run --rm --name wp2_rtabmaporb --network=host --privileged --volume=/home/pal/maps/:/home/ros/maps registry.gitlab.inria.fr/spring/dockers/wp2_rtabmap:orb_odom localization:=False database_path:=/home/ros/maps/running_map.db imu_topic:=/torso_front_camera/imu map_frame_id:=map odom_frame_id:=odom_ORB vo_frame_id:=odom_ORB guess_frame_id:=odom
