#!/usr/bin/env python
import rospy
import numpy as np
from dynamic_reconfigure.srv import Reconfigure,ReconfigureResponse
from dynamic_reconfigure.msg import Config, BoolParameter

def reset_AE():
    rospy.wait_for_service('/torso_front_camera/stereo_module/set_parameters')
    configuration = Config()
    param = BoolParameter()
    param.name = 'enable_auto_exposure'
    try:
        reset_AE = rospy.ServiceProxy('/torso_front_camera/stereo_module/set_parameters', Reconfigure)
        param.value=0 
        configuration.bools.append(param)
        set_OFF = reset_AE(configuration)
        
        rospy.loginfo("TURNED OFF")
        rospy.sleep(1)
        configuration.bools[0].value = 1
        set_ON = reset_AE(configuration)
    except rospy.ServiceException as e:
        print("Service call failed: %s"%e)
    
    rospy.loginfo("AE Reset finished. Closing now...")

if __name__ == '__main__':
    rospy.loginfo("Calling service")
    rospy.sleep(10)
    reset_AE()