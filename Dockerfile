FROM ros:noetic-ros-base as ros-noetic 
SHELL ["/bin/bash", "-c"]
WORKDIR /

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y keyboard-configuration curl && curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | apt-key add 
RUN apt-get update && apt-get install -y python3-pip \
                                        libeigen3-dev \
                                        python3-rospy \
                                        ros-noetic-tf \
                                        ros-noetic-cv-bridge \
					ros-noetic-perception \
                                        ros-noetic-image-transport-plugins \
                                        ros-noetic-imu-tools \
                                        ros-noetic-tf-conversions \
                                        ros-noetic-move-base-msgs \
                                        build-essential \
                                        git \
                                        libatlas-base-dev \
                                        libboost-all-dev \
                                        libgflags-dev \
                                        libprotobuf-dev \
                                        ocl-icd-opencl-dev \
                                        opencl-headers \
                                        pciutils \
                                        protobuf-compiler \
                                        python3-setuptools && \
                                        pip3 install \
                                        numpy \
                                        opencv-python \
                                        protobuf

# Download and install pangolin 
RUN git clone --recursive https://github.com/stevenlovegrove/Pangolin.git /root/pangolin
WORKDIR /root/pangolin 
RUN cmake -B build && cmake --build build && cd build && make install 

#Build orb slam 2:
COPY ./ORB_SLAM2 /root/ORB_SLAM2
WORKDIR /root/ORB_SLAM2
RUN export PATH=$PATH:/usr/include && chmod +x build.sh && ./build.sh && echo "ORB SLAM BUILD DONE"


#Download and build rtabmap with orb slam support:
ENV ORB_SLAM_ROOT_DIR=/root/ORB_SLAM2
# RUN git clone https://github.com/introlab/rtabmap.git /root/rtabmap 
# Install GTSAM
RUN apt-get install software-properties-common -y && add-apt-repository ppa:borglab/gtsam-release-4.0 && apt update && apt install libgtsam-dev libgtsam-unstable-dev -y
COPY ./rtabmap /root/rtabmap
WORKDIR /root/rtabmap/build
RUN cmake .. -DWITH_G2O=OFF -DWITH_QT=OFF -DWITH_ORB_SLAM=ON && make -j$(nproc) && make install

# Install rtabmap_ros
RUN mkdir -p /root/catkin_ws/src  
WORKDIR /root/catkin_ws
# RUN git clone -b noetic-devel https://github.com/introlab/rtabmap_ros.git /root/catkin_ws/src/rtabmap_ros  
COPY ./rtabmap_ros /root/catkin_ws/src/rtabmap_ros
# Package to turn AE on and initialize odom 
COPY ./odom_initializer/ /root/catkin_ws/src/odom_initializer/ 
# rtabmap localization diagnostics. 
COPY ./slam_diagnostics/ /root/catkin_ws/src/slam_diagnostics/
RUN apt-get install ros-noetic-costmap-2d -y 
RUN source /opt/ros/noetic/setup.bash && export LD_LIBRARY_PATH=/root/ORB_SLAM2/lib:/root/ORB_SLAM2/Thirdparty/g2o/lib:/root/ORB_SLAM2/Thirdparty/DBoW2/lib:$LD_LIBRARY_PATH && catkin_make -DCATKIN_BLACKLIST_PACKAGES="rtabmap_viz;rtabmap_rviz_plugins"

COPY ./entrypoint.sh /root/
COPY ./stereo_rtabmap_inria.launch /root/catkin_ws/src/rtabmap_ros/rtabmap_ros/launch/stereo_rtabmap_inria.launch
RUN chmod +x /root/entrypoint.sh
ENTRYPOINT ["/root/entrypoint.sh", "roslaunch", "rtabmap_ros", "stereo_rtabmap_inria.launch"]
CMD ["localization:=False", "database_name:=database"]