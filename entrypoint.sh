#!/bin/bash
echo $@
source /opt/ros/noetic/setup.bash
source /root/catkin_ws/devel/setup.bash
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/root/ORB_SLAM2/lib/:/root/ORB_SLAM2/Thirdparty/DBoW2/lib/:/root/ORB_SLAM2/Thirdparty/g2o/lib/
ldconfig 
rosrun odom_initializer reset_exposure_for_odom.py &
exec $@
