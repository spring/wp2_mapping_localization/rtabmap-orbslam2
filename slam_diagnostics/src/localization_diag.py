#!/usr/bin/env python

import rospy, rostopic
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus, KeyValue
from geometry_msgs.msg import PoseWithCovarianceStamped
from nav_msgs.msg import OccupancyGrid, Odometry
import sys

time_ok_min   = int(sys.argv[1]) 
time_ok_max   = int(sys.argv[2]) 
time_warn_max = int(sys.argv[3]) 

last_update = None

if __name__ == '__main__':
    rospy.init_node('rtabmap_diagnostics')
    r = rostopic.ROSTopicHz(-1)
    s = rospy.Subscriber('/slam/localization_pose', PoseWithCovarianceStamped, r.callback_hz, callback_args='/slam/localization_pose' )
    rospy.sleep(1)

    pub = rospy.Publisher('/diagnostics', DiagnosticArray, queue_size=1)
    arr = DiagnosticArray()

    rate = rospy.Rate(2)
    while not rospy.is_shutdown():
        arr.header.stamp = rospy.Time.now()
        avg_rate = r.get_hz('/slam/localization_pose')
        msg = DiagnosticStatus(level=DiagnosticStatus.ERROR, name='Navigation: Localization: slam_rtabmap', message='Hasn\'t localized in a long time. Possible error; Move to start position' )
        if avg_rate is not None:
            last_update = rospy.Time.now()

        if (last_update is not None):
            diff = (rospy.Time.now()-last_update).secs  
            if diff >= time_ok_min and diff <= time_ok_max:
                rospy.loginfo(diff)
                msg = DiagnosticStatus(level=DiagnosticStatus.OK, name='Navigation: Localization: slam_rtabmap', message="Last update " + str(diff) +" seconds ago")
            elif diff > time_ok_max and diff <= time_warn_max:
                msg = DiagnosticStatus(level=DiagnosticStatus.WARN, name='Navigation: Localization: slam_rtabmap', message='Hasn\'t localized in ' + str(diff) + ' seconds. Check if position is still OK')

        arr.status = [msg]
        pub.publish(arr)
        rate.sleep()


